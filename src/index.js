export class ZLogger {

    #name;

    constructor(name="ZOIB") {
        this.#name = name;
    }

    get name() {
        return this.#name;
    }

    set name(value) {
        this.#name = value;
        return this;
    }

    print(...toLog) {
        console.log(...toLog);
    }

    info(...toLog) {
        const _now = new Date().toISOString();
        console.log(`[${this.#name}] ${_now} info:`, ...toLog);
    }

    error(...toLog) {
        const _now = new Date().toISOString();
        console.error(`[${this.#name}] ${_now} error:`, ...toLog);
    }

    trace(error) {
        const _now = new Date().toISOString();
        console.error(`[${this.#name}] ${_now} error:`);
        console.trace(error);
    }

    endline() {
        console.log('');
    }
}